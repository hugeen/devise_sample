# 1 - Add devise to Gemfile:
  gem 'devise'

# 2 - Install the Gem:
  $ bundle install

# 3 - Install devise with the generator and read instructions:
  $ rails g devise:install

## 3.1 - Setup default url options for your specific environment. Here is an example of development environment:
  config.action_mailer.default_url_options = { :host => 'localhost:3000' }

## 3.2 - Ensure you have defined root_url to *something* in your config/routes.rb. For example:
  root :to => "home#index"

## 3.3 - Ensure you have flash messages in app/views/layouts/application.html.erb. For example:
  <p class="notice"><%= notice %></p>
  <p class="alert"><%= alert %></p>

## 3.4 - Change the mailer sender in config/initializers/devise.rb:
  config.mailer_sender = "please-change-me-at-config-initializers-devise@example.com"

# 4 - Create a new devise User model with the generator:
  $ rails g devise user

## 4.1 - Customize generated files with modules (See modules section below).

### 4.1.1 - Model - /app/models/user.rb, add or remove modules on this line:
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable

### 4.1.2 - Migration - /db/migrate/*_devise_create_users.rb, add or remove modules lines and migrate!
  $ rake db:migrate

# 5 - Look at routes.rb, we have a new line: devise_for :users. We have now access to these routes:
                  Prefix Verb   URI Pattern                    Controller#Action
        new_user_session GET    /users/sign_in(.:format)       devise/sessions#new
            user_session POST   /users/sign_in(.:format)       devise/sessions#create
    destroy_user_session DELETE /users/sign_out(.:format)      devise/sessions#destroy
           user_password POST   /users/password(.:format)      devise/passwords#create
       new_user_password GET    /users/password/new(.:format)  devise/passwords#new
      edit_user_password GET    /users/password/edit(.:format) devise/passwords#edit
                         PATCH  /users/password(.:format)      devise/passwords#update
                         PUT    /users/password(.:format)      devise/passwords#update
cancel_user_registration GET    /users/cancel(.:format)        devise/registrations#cancel
       user_registration POST   /users(.:format)               devise/registrations#create
   new_user_registration GET    /users/sign_up(.:format)       devise/registrations#new
  edit_user_registration GET    /users/edit(.:format)          devise/registrations#edit
                         PATCH  /users(.:format)               devise/registrations#update
                         PUT    /users(.:format)               devise/registrations#update
                         DELETE /users(.:format)               devise/registrations#destroy
                    root GET    /                              home#index

## 5.1 - Create Login links into our layout (application.hml.erb):

  <% if user_signed_in? %>
    Logged in as <strong><%= current_user.email %></strong>.
    <%= link_to 'Edit profile', edit_user_registration_path %> |
    <%= link_to "Logout", destroy_user_session_path, method: :delete %>
  <% else %>
    <%= link_to "Sign up", new_user_registration_path %> |
    <%= link_to "Login", new_user_session_path %>
  <% end %>
  
  <% flash.each do |name, msg| %>
    <%= content_tag :div, msg, id: "flash_#{name}" %>
  <% end %>
    
  <%= yield %>
  
## 5.2 - We can now start the server (rails s) and Sign up, Sign in, Login, Edit profile and Recover Password.

# 6 - Edit config/locales/devise.en.yml to customize flash messages.

# 7 - Customize defaults Devise views:

## 7.1 - First generate views with the generator:
  rails g devise:views

## 7.2 - We can now customize all devise views in app/views/devise.

# 8 - (Customize routes) Replace the users/sign_in route by users/login (routes.rb):
  devise_for :users, path_names: {sign_in: "login", sign_out: "logout"}

# 9 - Verify authentication into controllers, example:

  class ArticlesController < ApplicationController
    before_filter :authenticate_user!, except: [:index, :show]
  end

# Modules:

  * Database Authenticatable: (http://rubydoc.info/github/plataformatec/devise/master/Devise/Models/DatabaseAuthenticatable)
      encrypts and stores a password in the database to validate the authenticity of a user while signing in.
      The authentication can be done both through POST requests or HTTP Basic Authentication.
      
  * Token Authenticatable: (http://rubydoc.info/github/plataformatec/devise/master/Devise/Models/TokenAuthenticatable)
      signs in a user based on an authentication token (also known as "single access token").
      The token can be given both through query string or HTTP Basic Authentication.
      
  * Omniauthable: (http://rubydoc.info/github/plataformatec/devise/master/Devise/Models/Omniauthable)
      adds Omniauth (https://github.com/intridea/omniauth) support;
      
  * Confirmable: (http://rubydoc.info/github/plataformatec/devise/master/Devise/Models/Confirmable)
      sends emails with confirmation instructions and verifies whether an account is already confirmed during sign in.
      
  * Recoverable: (http://rubydoc.info/github/plataformatec/devise/master/Devise/Models/Recoverable)
      resets the user password and sends reset instructions.
      
  * Registerable: (http://rubydoc.info/github/plataformatec/devise/master/Devise/Models/Registerable)
      handles signing up users through a registration process, also allowing them to edit and destroy their account.
      
  * Rememberable: (http://rubydoc.info/github/plataformatec/devise/master/Devise/Models/Rememberable)
      manages generating and clearing a token for remembering the user from a saved cookie.
      
  * Trackable: (http://rubydoc.info/github/plataformatec/devise/master/Devise/Models/Trackable)
      tracks sign in count, timestamps and IP address.
      
  * Timeoutable: (http://rubydoc.info/github/plataformatec/devise/master/Devise/Models/Timeoutable)
      expires sessions that have no activity in a specified period of time.
      
  * Validatable: (http://rubydoc.info/github/plataformatec/devise/master/Devise/Models/Validatable)
      provides validations of email and password.
      It's optional and can be customized, so you're able to define your own validations.
      
  * Lockable: (http://rubydoc.info/github/plataformatec/devise/master/Devise/Models/Lockable)
      locks an account after a specified number of failed sign-in attempts.
      Can unlock via email or after a specified time period.
  